class Word < ActiveRecord::Base
  def read_points
    (Time.now.utc.to_date - self.read_last_date.to_date).to_i
  end

  def write_points
    (Time.now.utc.to_date - self.write_last_date.to_date).to_i
  end
end
