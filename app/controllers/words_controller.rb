class WordsController < ApplicationController

  def index

  end

  def submit
    words = JSON.parse(params["words"])
    words.each do |word|
      w = Word.find_by!(name: word["name"])
      if word["mode"] == 0
        w.read_last_date = word["points"].days.ago
      else
        w.write_last_date = word["points"].days.ago
      end
      w.save!
    end
  end

end
