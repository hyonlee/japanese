Rails.application.routes.draw do
  post '/submit', to: 'words#submit'

  root 'words#index'
end
