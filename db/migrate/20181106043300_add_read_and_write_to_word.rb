class AddReadAndWriteToWord < ActiveRecord::Migration
  def change
    add_column :words, :read_last_date, :string
    add_column :words, :write_last_date, :string
    remove_column :words, :last_seen
  end
end
